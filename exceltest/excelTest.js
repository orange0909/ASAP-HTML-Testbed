window.onload = () => {
    let fileInputElement = document.querySelector('input[type="file"]');
    let resultElement = document.querySelector('div#result');

    if(fileInputElement != null) {
        fileInputElement.addEventListener('change', (event) => {
            let reader = new FileReader();

            reader.onload = () => {
                let data = reader.result;
                let workBook = XLSX.read(data, {
                    type: 'binary'
                });

                workBook.SheetNames.forEach((sheetName) => {
                    console.log(sheetName);

                    let rows = XLSX.utils.sheet_to_json(workBook.Sheets[sheetName]);
                    
                    let tableElement = document.createElement('table');
                    resultElement.appendChild(tableElement);
                    
                    // 데이터가 있는 가장 윗줄을 Row Name으로 인식함
                    // Row Name은 for of 했을 때 key값으로 추출 가능
                    rows.forEach((row, index) => {
                        if(index === 0) {
                            let rowElement = document.createElement('tr');
                            tableElement.appendChild(rowElement);
                            
                            for (const [key, value] of Object.entries(row)) {
                                let columnElement = document.createElement('th');
                                rowElement.appendChild(columnElement);
                                
                                columnElement.innerText = key;
                            }
                        }
                        
                        let rowElement = document.createElement('tr');
                        tableElement.appendChild(rowElement);

                        for (const [key, value] of Object.entries(row)) {
                            let columnElement = document.createElement('td');
                            rowElement.appendChild(columnElement);
    
                            columnElement.innerText = value;
                        }
                    });
                });
            };

            reader.readAsBinaryString(fileInputElement.files[0]);
        });
    }
};