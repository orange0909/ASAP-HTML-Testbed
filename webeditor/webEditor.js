
window.onload = () => {
    tinymce.init({
        selector: 'textarea#default-editor',
        width: '600px',
        height: '400px',
        plugins: [
            'advlist', 'autolink', 'link', 'image', 'lists', 'charmap', 'preview', 'anchor', 'pagebreak',
            'searchreplace', 'wordcount', 'visualblocks', 'visualchars', 'code', 'fullscreen', 'insertdatetime',
            'media', 'table', 'emoticons', 'template', 'help'
          ],
        toolbar: 'undo redo | styles | bold italic | alignleft aligncenter alignright alignjustify | ' +
          'bullist numlist outdent indent | link image | print preview media fullscreen | ' +
          'forecolor backcolor emoticons | help',
        menubar: 'file edit view insert format tools table help'
    }).then(() => {
		let editor = tinymce.get('default-editor');
		let result = document.querySelector('div#result');

		editor.on('keyup', () => {
			result.innerText = editor.getContent();
		});
	});

    let btn = document.querySelector('button#copy');
    btn.addEventListener('click', () => {
        let editorContent = tinymce.get('default-editor').getContent();
        let result = document.querySelector('div#result');
        // result.textContent = editorContent;
        result.innerText = editorContent;
    });

};